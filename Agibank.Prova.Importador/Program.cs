﻿using Agibank.Prova.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Agibank.Prova.Importador
{
    class Program
    {

        static ArquivoImportService serviceImport;
        static System.IO.FileSystemWatcher watcher;
        static string pathToWrite;
        static string pathToWatch;
        //static Dire
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando Importador..");

            StartProcess();
        }

        static void StartProcess()
        {
            serviceImport = new ArquivoImportService();
            pathToWrite = string.Format("{0}\\{1}", AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["PathToWrite"]);
            pathToWatch = string.Format("{0}\\{1}", AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["PathToWatch"]);

            CreateDirectories();

            watcher = new FileSystemWatcher();
            watcher.Path = pathToWatch;

            GC.KeepAlive(pathToWrite);
            GC.KeepAlive(serviceImport);
            GC.KeepAlive(watcher);


            watcher.Created += Watcher_Created;
            watcher.EnableRaisingEvents = true;
            Console.WriteLine("Observando a pasta Importador.." + pathToWatch);
            Console.ReadKey();
        }


        static void CreateDirectories()
        {
            
            if (!Directory.Exists(pathToWatch))
                Directory.CreateDirectory(pathToWatch);

            if (!Directory.Exists(pathToWrite))
                Directory.CreateDirectory(pathToWrite);
        }

        private static void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            if(e.ChangeType == WatcherChangeTypes.Created)
            {
                try
                {
                    Console.WriteLine(string.Format("Importanto Arquivo: {0}", e.Name));
                    var file = serviceImport.ProcessFile(e.FullPath);
                    DateTime startedTime = DateTime.Now;
                    serviceImport.WriteFile(file, string.Format("{0}\\{1}", pathToWrite, e.Name));
                    Console.WriteLine("Processo Finalizado em {0} segundos", (DateTime.Now - startedTime).TotalSeconds);
                    if(file.LinesErrors != null)
                    {
                        Console.WriteLine("Não foram processados {0} linhas com errors.", file.LinesErrors.Count);
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Ocorreu um erro ao processar o arquivo " + e.Name);
                    Console.WriteLine("--------ERRO-----------");
                    Console.WriteLine(ex.Message);

                }
            }
            
        }
    }
}
