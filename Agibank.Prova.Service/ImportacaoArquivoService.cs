﻿using Agibank.Prova.Repository;
using Aigbank.Prova.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agibank.Prova.Service
{
    public class ArquivoImportService
    {
        public RepositoryArquivo<Arquivo> repository;

        public ArquivoImportService()
        {
            this.repository = new RepositoryArquivo<Arquivo>();
        }

        public Arquivo ProcessFile(string filePath)
        {
            var file = this.repository.ReadFile(filePath);

            file.ProcessLines();

            return file;
           
        }

        public void WriteFile(Arquivo file,string filePath)
        {
            this.repository.WriteOutput(file,filePath);
        }
    }
}
