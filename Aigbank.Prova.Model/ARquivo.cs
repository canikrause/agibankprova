﻿using Aigbank.Prova.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aigbank.Prova.Model
{
    public class Arquivo : IArquivo
    {

        public Arquivo()
        {
            
        }
        
        public List<string> LinesErrors { get; set; }

        public List<Vendedor> Vendedores { get; private set; }
        public List<Cliente> Clientes { get; private set; }
        public List<Venda> Vendas { get; private set; }
        public string NameFile { get; set; }

        public StreamReader Reader { get; set; }
            
        

        #region Métodos Privados

        /// <summary>
        /// Processa as a linha identificando o tipo dela
        /// </summary>
        /// <param name="linha"></param>
        private void ProcessLine(string linha)
        {
            var type = linha.Substring(0, 3);
            string[] linhaInterpretada = ParseLine(linha);
            switch (type)
            {
                case "001":
                    //Vendedor
                    if (this.Vendedores == null)
                        this.Vendedores = new List<Vendedor>();

                    var venderdor = ProcessVendedor(linhaInterpretada);
                    this.Vendedores.Add(venderdor);
                    break;
                case "002":
                    //Cliente
                    if (this.Clientes == null)
                        this.Clientes = new List<Cliente>();
                    var cliente = ProcessCliente(linhaInterpretada);
                    this.Clientes.Add(cliente);

                    break;
                case "003":
                    //Venda
                    if (this.Vendas == null)
                        this.Vendas = new List<Venda>();
                    var venda = ProcessVenda(linhaInterpretada);
                    this.Vendas.Add(venda);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Processo Linha de Vendedor
        /// </summary>
        /// <param name="valores"></param>
        /// <returns></returns>
        private Vendedor ProcessVendedor(string[] valores)
        {

            string cpf = valores[1];
            string name = valores[2];
            double salary = 0;
            double.TryParse(valores[3], out salary);

            return new Vendedor(cpf, name, salary);

        }

        /// <summary>
        /// Processa Linha de Cliente
        /// </summary>
        /// <param name="valores"></param>
        /// <returns></returns>
        private Cliente ProcessCliente(string[] valores)
        {
            string cpf = valores[1];
            string name = valores[2];
            string businessArea = valores[3];

            return new Cliente(cpf, name, businessArea);
        }

        /// <summary>
        /// Processa Linha de Venda
        /// </summary>
        /// <param name="valores"></param>
        /// <returns></returns>
        private Venda ProcessVenda(string[] valores)
        {
            string salesId = valores[1];
            string salesMan = valores[3];
            string itens = valores[2];
            return new Venda(salesId, salesMan, itens);
        }

        /// <summary>
        /// Realiza o Parse da liha separando por cedilha
        /// </summary>
        /// <param name="linha"></param>
        /// <returns></returns>
        private string[] ParseLine(string linha)
        {
            //var valores = linha.Split('ç');

            int size = 1;
            string[] values = new string[size];



            int lastSeparator = 0;
            for (int i = 0; i < linha.Length; i++)
            {
                bool ditongo = false;
                var nextSeparator = linha.IndexOf('ç', i);

                if (nextSeparator == 0)
                {
                    values[size - 1] = string.Empty;
                    size++;
                    string[] newArray = new string[size];
                    values.CopyTo(newArray, 0);
                    values = newArray;
                    continue;
                }
                else
                {
                    var letterAfterSeparator = linha[nextSeparator + 1];
                    var letterBeforeSeparator = (nextSeparator != -1) ? linha[nextSeparator - 1] : ' ';
                    if (!IsAOU(letterAfterSeparator))
                    {
                        if (nextSeparator == -1)
                        {
                            //values[size - 1] = linha.Substring(i, linha.Length - i);
                            ProcessDitongo(ref values, size, linha.Substring(i, linha.Length - i), linha, i, nextSeparator, out ditongo);
                            i = linha.Length;
                            continue;
                        }
                        else
                        {
                            var subValue = linha.Substring(i, nextSeparator - i);
                            //values[size-1] = linha.Substring(i, nextSeparator - i);

                            if (!HasMoreSeparator(linha, nextSeparator))
                            {
                                values[size - 1] = linha.Substring(i);
                                i = linha.Length;
                                continue;
                            }

                            ProcessDitongo(ref values, size, subValue, linha, i, nextSeparator, out ditongo);
                        }

                        if (!ditongo)
                        {
                            size++;
                            string[] newArray = new string[size];
                            values.CopyTo(newArray, 0);
                            values = newArray;
                        }
                        i = nextSeparator;
                        lastSeparator = nextSeparator;
                        continue;
                    }
                    else if (!IsAOU(letterBeforeSeparator) && !IsAfterAOU(letterAfterSeparator))
                    {
                        values[size - 1] = linha.Substring(i, nextSeparator - i);
                        size++;
                        string[] newArray = new string[size];
                        values.CopyTo(newArray, 0);
                        values = newArray;
                        i = nextSeparator;
                        lastSeparator = nextSeparator;
                        continue;
                    }
                    else
                    {

                        if (IsAOU(letterBeforeSeparator) && IsAfterAOU(letterAfterSeparator))
                        {
                            var l = linha[nextSeparator - 2];

                            if (IsNoVogal(l))
                            {
                                nextSeparator = linha.IndexOf('ç', nextSeparator);

                                string subValue = linha.Substring(i, nextSeparator - i);


                                ProcessDitongo(ref values, size, subValue, linha, i, nextSeparator, out ditongo);


                                if (!HasMoreSeparator(linha, nextSeparator + 1))
                                {
                                    values[size - 1] = linha.Substring(i);
                                    i = linha.Length;

                                }
                                else
                                {

                                    if (!ditongo)
                                    {
                                        size++;
                                        string[] newArray = new string[size];
                                        values.CopyTo(newArray, 0);
                                        values = newArray;
                                    }
                                    i = nextSeparator;
                                    lastSeparator = nextSeparator;
                                }

                            }

                        }

                    }
                }
            }

            return values;

            #region old
            //char prevLetter = ' ';
            //char currentLetter = ' ';
            //string currentLot = "";

            //int size = 1;
            //string[] values = new string[size];
            //int lastPositionLetter = linha.Length - 1;
            //for (int postionLetter = 0; postionLetter < linha.Length; postionLetter++)
            //{
            //    currentLetter = linha[postionLetter];
            //    char nextLetter = (lastPositionLetter == postionLetter) ? ' ' : linha[postionLetter + 1];



            //    if (prevLetter == ' ')
            //    {
            //        if (currentLetter.Equals('ç'))
            //        {
            //            values[size - 1] = string.Empty;
            //            size++;
            //            string[] newArray = new string[size];
            //            values.CopyTo(newArray, 0);
            //            values = newArray;
            //            prevLetter = currentLetter;
            //            continue;

            //        }

            //        currentLot += currentLetter;
            //        prevLetter = currentLetter;
            //        continue;
            //    }
            //    else
            //    {
            //        if(currentLetter.Equals('ç') && !IsAOEU(nextLetter.ToString()))
            //        {
            //            //fim do dado
            //            values[size - 1] = currentLot;
            //            size++;
            //            string[] newArray = new string[size];
            //            values.CopyTo(newArray, 0);
            //            values = newArray;
            //            prevLetter = currentLetter;
            //            currentLot = string.Empty;
            //            continue;

            //        }
            //        if (currentLetter.Equals('ç') && IsAOEU(prevLetter.ToString()) && IsAOEU(nextLetter.ToString()))
            //        {
            //            //palavra com cedilha
            //            //aça Te

            //            if(postionLetter+1 == lastPositionLetter)
            //            {
            //                //fim dalinha so agrega e contiua
            //                currentLot += currentLetter;
            //                prevLetter = currentLetter;
            //                continue;
            //            }
            //            else if(!IsAOEU(linha[postionLetter + 2].ToString()))
            //            {

            //                //cedilha é fim de dado
            //                //fim do dado
            //                values[size - 1] = currentLot;
            //                size++;
            //                string[] newArray = new string[size];
            //                values.CopyTo(newArray, 0);
            //                values = newArray;
            //                prevLetter = currentLetter;
            //                currentLot = string.Empty;
            //                continue;
            //            }
            //            else
            //            {
            //                currentLot += currentLetter;
            //                prevLetter = currentLetter;
            //            }

            //        }
            //        if (currentLetter.Equals('ç') && !IsAOEU(prevLetter.ToString()))
            //        {
            //            //fim do dado
            //            values[size - 1] = currentLot;
            //            size++;
            //            string[] newArray = new string[size];
            //            values.CopyTo(newArray, 0);
            //            values = newArray;
            //            prevLetter = currentLetter;
            //            currentLot = string.Empty;
            //            continue;
            //        }
            //        else
            //        {
            //            currentLot += currentLetter;
            //            prevLetter = currentLetter;
            //        }
            //    }
            //}

            //if (!string.IsNullOrEmpty(currentLot))
            //    values[values.Length - 1] = currentLot;

            //return values;

            #endregion
        }

        
        public void ProcessDitongo(ref string[] currentArray, int currentSize, string subValue, string line, int currentPosition, int seperator, out bool ditongo)
        {
            ditongo = false;
            if (subValue.Contains("ão"))
            {
                if (subValue.Equals("ão", StringComparison.InvariantCultureIgnoreCase))  //ditongos 
                {
                    if (seperator == -1)
                    {
                        currentArray[currentSize - 2] += line.Substring(currentPosition - 1, line.Length - currentPosition + 1);
                    }
                    else
                    {
                        currentArray[currentSize - 2] += line.Substring(currentPosition - 1, seperator - currentPosition + 1);
                    }
                    ditongo = true;

                }
                else
                {
                    if (!subValue.Contains("rão"))
                    {
                        var newsubValues = line.Substring(currentPosition - 1, seperator - currentPosition + 1);
                        if (newsubValues.StartsWith("ç"))
                        {
                            currentArray[currentSize - 2] += newsubValues;
                            ditongo = true;
                        }
                    }
                    else
                        currentArray[currentSize - 1] = subValue;
                }
                //hasDitongo = true;

            }
            else
                currentArray[currentSize - 1] = subValue;

        }

        public bool HasMoreSeparator(string linha, int from)
        {
            return linha.IndexOf('ç', from) > -1;
        }

        private bool IsAOU(char letter)
        {
            string[] letters = new string[] { "a", "o", "u" };
            foreach (var item in letters)
            {
                if (item.Equals(letter.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }

            return false;
        }

        private bool IsNoVogal(char letter)
        {
            string[] letters = new string[] { "a", "e", "i", "o", "u", "ç" };
            foreach (var item in letters)
            {
                if (item.Equals(letter.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    return false;
            }

            return true;
        }

        private bool IsAfterAOU(char letter)
        {
            string[] letters = new string[] { "a", "á", "o", "ã", "u" };
            foreach (var item in letters)
            {
                if (item.Equals(letter.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }

            return false;
        }

        #endregion


        #region Métodos Publicos

        /// <summary>
        /// Processa as linas contidas no arquivo
        /// </summary>
        public void ProcessLines()
        {
            if (this.Reader == null)
                throw new Exception("Por favor, carregue as linhas a partir de um arquivo");


            try
            {
                while (this.Reader.Peek() > -1)
                {
                    string linha = this.Reader.ReadLine();
                    try
                    {
                        this.ProcessLine(linha);
                    }
                    catch (Exception ex)
                    {
                        if (this.LinesErrors == null)
                            this.LinesErrors = new List<string>();

                        this.LinesErrors.Add(linha);
                    }
                }

                
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Reader.Close();
            }

           
        }

        /// <summary>
        /// Escrteve a Saida do Arquivo
        /// </summary>
        /// <returns></returns>
        public string OutputFile()
        {
            
            int sizeClientes = this.Clientes != null ? this.Clientes.Count : 0;
            int sizeVendedores = this.Vendedores != null ? this.Vendedores.Count : 0;
            string idExpensiveVenda = string.Empty;
            string worseVendedor = string.Empty;

            if (this.Vendas != null && this.Vendedores != null)
            {
                var VendasGroupByVendedor = (from v in this.Vendas
                                             join ve in this.Vendedores on v.SalesManName equals ve.Name
                                             group v by v.SalesManName into grp
                                             select new
                                             {
                                                 Nome = grp.Key,
                                                 Total = grp.Sum(v => v.GetTotalAmount())
                                             }).OrderByDescending(v => v.Total);


                var vendasOrderByAmountDesc = this.Vendas.OrderByDescending(v => v.GetTotalAmount());

                idExpensiveVenda = vendasOrderByAmountDesc.FirstOrDefault().Id;
                worseVendedor = VendasGroupByVendedor.LastOrDefault().Nome;
            }

            return string.Format("{0}\n{1}\n{2}\n{3}", sizeClientes, sizeVendedores, idExpensiveVenda, worseVendedor);
        }

        /// <summary>
        /// Escreve a Saida dos Erros
        /// </summary>
        /// <returns></returns>
        public string OutputErrors()
        {
           

            if (this.LinesErrors != null)
                if(this.LinesErrors.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    this.LinesErrors.ForEach((erro) => { sb.AppendLine(erro); });

                    return sb.ToString();
                }

            return string.Empty;
        }


        #endregion
    }
}
