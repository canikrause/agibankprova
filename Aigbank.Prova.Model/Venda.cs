﻿using Aigbank.Prova.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aigbank.Prova.Model
{
    public class Venda : IAggregateModel
    {
        private Venda()
        {

        }

        public Venda(string salesId, string salesManName,string itens)
        {
            this.Id = salesId;

            this.SalesManName = salesManName;

            this.ParseItens(itens);
        }

        public string Id { get;  }

        public string SalesManName { get; set; }

        public List<ItemVenda> Itens {get; private set;}

        public double GetTotalAmount()
        {
            if (this.Itens == null || (this.Itens != null && this.Itens.Count == 0))
                return 0;

            double amount = this.Itens.Sum(i => i.Price * i.Quantity);

            return amount;
        }

        #region Métodos Privados

        public void ParseItens(string itens)
        {
            if(string.IsNullOrEmpty(itens))
            {
                return;
            }

            if(!itens.Substring(0,1).Equals("[",StringComparison.InvariantCultureIgnoreCase) || !itens.Substring(itens.Length-1,1).Equals("]"))
            {
                throw new Exception("Não foi possivel fazer o parse dos itens pois não estão no formato correto");
            }

            if (this.Itens == null)
                this.Itens = new List<ItemVenda>();

            var arrayOfItens = itens.Replace("[", "").Replace("]", "").Split(',');
            foreach (var item in arrayOfItens)
            {
                this.Itens.Add(ItemVenda.Parse(item));
            }
            
        }

        #endregion
    }
}
