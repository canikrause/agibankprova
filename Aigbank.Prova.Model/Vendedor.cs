﻿using Aigbank.Prova.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aigbank.Prova.Model
{
    public class Vendedor : IAggregateModel
    {

        private Vendedor()
        {

        }

        public Vendedor(string cpf, string name, double salary) : base()
        {
            this.CPF = cpf;
            this.Name = name;
            this.Salary = salary;
        }


        public string CPF { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }

        public string Id { get; }
    }
}
