﻿using Aigbank.Prova.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aigbank.Prova.Model
{
    public class ItemVenda : IAggregateModel
    {
        private ItemVenda()
        {

        }


        public ItemVenda(string id,int quantity,double price)
        {
            this.Id = id;
            this.Quantity = quantity;
            this.Price = price;
        }



        public string Id { get; }
        public int Quantity { get; set; }

        public double Price { get; set; }


        #region Métodos Públbicos

        public static ItemVenda Parse(string value)
        {
            var splitValues = value.Split('-');

            return new ItemVenda(splitValues[0], int.Parse(splitValues[1]), double.Parse(splitValues[2])) ;
        }

        #endregion
    }
}
