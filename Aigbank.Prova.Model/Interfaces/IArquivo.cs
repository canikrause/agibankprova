﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aigbank.Prova.Model.Interfaces
{
    public interface IArquivo
    {
        
        StreamReader Reader { get; set; }
        List<string> LinesErrors { get; set; }

        string NameFile { get; set; }
        void ProcessLines();
        string OutputFile();

        string OutputErrors();
    }
}
