﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aigbank.Prova.Model.Interfaces
{
    public interface IAggregateModel
    {
        string Id { get; }
    }
}
