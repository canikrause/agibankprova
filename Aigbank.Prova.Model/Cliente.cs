﻿using Aigbank.Prova.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aigbank.Prova.Model
{
    public class Cliente : IAggregateModel
    {
        private Cliente()
        {

        }

        public Cliente(string cpf, string name, string businessArea) : base()
        {
            this.CPF = cpf;
            this.Name = name;
            this.BusinessArea = businessArea;
        }


        public string CPF { get; set; }
        public string Name { get; set; }
        public string BusinessArea { get; set; }

        public string Id { get; }
    }
}

