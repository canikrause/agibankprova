
# Prova - Importador arquivo

Esse projeto � a cria��o de um aplicativo conforme solicitado na prova.
O projeto est� montado utilizando Domain Drive Design.

## Estrutura Projeto
Esse projeto est� estruturado da seguinte forma:

* Agibank.Prova.Importador  - **Projeto que cont�m o execut�vel para Testar**
* Agibank.Prova.Repository - **Projeto que representa o repository do Domain Drive Design, fazendo as escritas nos arquivo**
* Agibank.Prova.Service - **Projeto de Servi�o que cont�m o Servi�o de importa��o**
* Agibank.Prova.Test - **Projeto de Testes**
* Agibank.Prova.Model - **Projeto que cont�m os modelos de dados para ser utilizados nos reposit�rios e servi�o**

No projeto **Agibank.Prova.Importador** dentro do app.config do execut�vel � poss�vel definir o nome da pasta que ir� ser criada para leitura e exporta��o do resultados. 

```
 <!-- Caminho para Leitura -->
    <add key="PathToWatch" value="data\\in"/>
    <!-- Caminho para Escritra -->
    <add key="PathToWrite" value="data\\out"/>
```

Em caso de falha na leitura de uma linha ele reporta as linhas na pasta de saida com a seguinte nomenclatura: **errors_<arquivo>**


**Michael Pereira**



