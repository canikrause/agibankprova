﻿using Aigbank.Prova.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agibank.Prova.Repository.Interfaces
{
    public interface IRepositoryVendedor<T> : IRepositoryBase<T> where T : IAggregateModel
    {
    }
}
