﻿using Agibank.Prova.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aigbank.Prova.Model;
using Aigbank.Prova.Model.Interfaces;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading;

namespace Agibank.Prova.Repository
{
    public class RepositoryArquivo<T> : IRepositoryArquivo<T> where T: IArquivo
    {
        /// <summary>
        /// Realiza a leitura do arquivo
        /// </summary>
        /// <param name="caminho"></param>
        /// <returns></returns>
        public T ReadFile(string caminho)
        {
            IArquivo arquivo = (IArquivo)Activator.CreateInstance<T>();

            int retries = 0;

            while (retries < 3)
            {
                try
                {
                    arquivo.Reader = new StreamReader(caminho);
                    arquivo.NameFile = Regex.Match(caminho, @"[^\\]+(?=(?:\.[^.]+)?$)").Value;
                    break;
                }
                catch
                {
                    Thread.Sleep(1000);
                    retries++;
                }
            }

            return (T)arquivo;
        }

        /// <summary>
        /// Escreve a saida e os erros do arquivo
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="filePath"></param>
        public void WriteOutput(T arquivo,string filePath)
        {
            var objArquivo = ((IArquivo)arquivo);
            var output = objArquivo.OutputFile();

            using (StreamWriter writer = new StreamWriter(filePath, false))
            {
                writer.WriteLine(output);
                writer.Flush();
                writer.Close();
            }

            var errors = objArquivo.OutputErrors();
            if(!string.IsNullOrEmpty(errors))
            {
               var filePathError = filePath.Replace(objArquivo.NameFile, "errors_" + objArquivo.NameFile);
                using (StreamWriter writer = new StreamWriter(filePathError, false))
                {
                    writer.WriteLine(output);
                    writer.Flush();
                    writer.Close();
                }
            }
        }
    }
}
