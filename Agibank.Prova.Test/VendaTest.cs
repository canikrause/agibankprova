﻿using System;
using Aigbank.Prova.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Agibank.Prova.Test
{
    [TestClass]
    public class VendaTest
    {
        [TestMethod]
        public void TestCreateNewObject()
        {
            string salesId = "003";
            string itens = "[1-10-100,2-30-2.50,3-40-3.10]";
            string salesMan = "Pedro";

            Venda objectVenda = new Venda(salesId, salesMan, itens);

            Assert.AreEqual(salesId, objectVenda.Id);
            Assert.AreEqual(salesMan, objectVenda.SalesManName);
            Assert.IsTrue(objectVenda.Itens.Count == 3);


        }
    }
}
