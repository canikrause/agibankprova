﻿using System;
using Agibank.Prova.Repository;
using Aigbank.Prova.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Agibank.Prova.Test
{
    [TestClass]
    public class ArquivoTeste
    {
        private RepositoryArquivo<Arquivo> repositoryArquivo;

        [TestInitialize]
        public void Initialize()
        {
            if (repositoryArquivo == null)
                repositoryArquivo = new RepositoryArquivo<Arquivo>();
        }

        [TestMethod]
        public void InterpretarLinhaSucesso()
        {
            PrivateObject arquivoObject = new PrivateObject(typeof(Arquivo));
            var result = arquivoObject.Invoke("InterpretarLinha", "002ç2345675433444345çEduardo PereiraçAraça Teste");

            Assert.IsTrue(result.GetType() == typeof(string[]));
        }


        [TestMethod]
        public void ReadFile()
        {
            var arquivo = repositoryArquivo.ReadFile(string.Format("{0}\\data\\in\\{1}", AppDomain.CurrentDomain.BaseDirectory, "arquivo1.txt"));

            Assert.IsNotNull(arquivo.Reader);
        }

        [TestMethod]
        public void ProcessLines()
        {
            var arquivo = repositoryArquivo.ReadFile(string.Format("{0}\\data\\in\\{1}", AppDomain.CurrentDomain.BaseDirectory, "arquivo1.txt"));

            arquivo.ProcessLines();

            Assert.IsNotNull(arquivo);
            Assert.IsTrue(arquivo.Vendas.Count == 2);
            Assert.IsTrue(arquivo.Clientes.Count == 2);
            Assert.IsTrue(arquivo.Vendedores.Count == 2);
        }
        
        [TestMethod]
        public void GenerateOutput()
        {
            var arquivo = repositoryArquivo.ReadFile(string.Format("{0}\\data\\in\\{1}", AppDomain.CurrentDomain.BaseDirectory, "arquivo1.txt"));

            arquivo.ProcessLines();

            var saida = arquivo.OutputFile();
        }
    }
}
