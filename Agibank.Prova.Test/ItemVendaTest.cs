﻿using System;
using Aigbank.Prova.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Agibank.Prova.Test
{
    [TestClass]
    public class ItemVendaTest
    {
        [TestMethod]
        public void TestParse()
        {
            string value = "1-10-10";
            ItemVenda item = ItemVenda.Parse(value);

            Assert.AreEqual("1", item.Id);
            Assert.AreEqual(10, item.Quantity);
            Assert.AreEqual(10.2, item.Price);
        }
    }
}
